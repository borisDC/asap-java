package com.atlassian.asap.core.keys.privatekey;

import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.core.keys.DataUriKeyReader;
import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.validator.ValidatedKeyId;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ProvideSystemProperty;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.InputStreamReader;
import java.net.URI;
import java.security.PrivateKey;
import java.security.interfaces.RSAPrivateKey;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertSame;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

public class SystemPropertyKeyProviderTest {
    private static final String VALID_KID = "issuer/kid";
    private static final String PROPERTY_NAME_1 = "property.name.1";
    private static final String PROPERTY_NAME_2 = "property.name.2";
    private static final URI VALID_URL = URI.create("sysprop:///" + PROPERTY_NAME_1);
    private static final URI INVALID_URL = URI.create("sysprop:///" + PROPERTY_NAME_2);
    private static final String VALID_KEY_DATA = "data:application/pkcs8;kid=issuer%2Fkid;base64,EncodedKeyData";
    private static final String UNPARSEABLE_PRIVATE_KEY_DATA = "unparseable private key data uri";

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Rule
    public final ProvideSystemProperty provideSystemProperty1 =
            new ProvideSystemProperty(PROPERTY_NAME_1, VALID_KEY_DATA);

    @Rule
    public final ProvideSystemProperty provideSystemProperty2 =
            new ProvideSystemProperty(PROPERTY_NAME_2, UNPARSEABLE_PRIVATE_KEY_DATA);

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Mock
    private DataUriKeyReader keyReader;
    @Mock
    private RSAPrivateKey privateKey;

    @Test
    public void shouldBeAbleToReadKeyFromSystemProperty() throws Exception {
        when(keyReader.readPrivateKey(any(InputStreamReader.class))).thenReturn(privateKey);

        KeyProvider<PrivateKey> keyProvider = SystemPropertyKeyProvider.fromUri(VALID_URL, keyReader);

        assertSame(privateKey, keyProvider.getKey(ValidatedKeyId.validate(VALID_KID)));
    }

    @Test
    public void shouldFailWhenKeyIsNotAUri() throws Exception {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(not(containsString("private key")));

        KeyProvider<PrivateKey> keyProvider = SystemPropertyKeyProvider.fromUri(INVALID_URL, keyReader);
        keyProvider.getKey(ValidatedKeyId.validate(VALID_KID));
    }

    @Test
    public void shouldFailWhenKeyParserFails() throws Exception {
        when(keyReader.readPrivateKey(any(InputStreamReader.class)))
                .thenThrow(new CannotRetrieveKeyException("Random error"));

        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(not(containsString(VALID_KEY_DATA)));

        // we don't care whether it fails on construction (eager) or on use (lazy)
        KeyProvider<PrivateKey> keyProvider = SystemPropertyKeyProvider.fromUri(VALID_URL, keyReader);
        keyProvider.getKey(ValidatedKeyId.validate(VALID_KID));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailWhenSystemPropertyIsNotDefined() throws Exception {
        KeyProvider<PrivateKey> keyProvider = SystemPropertyKeyProvider.fromUri(URI.create("sysprop:///asap.key"), keyReader);
        keyProvider.getKey(ValidatedKeyId.validate(VALID_KID));
    }

    @Test(expected = CannotRetrieveKeyException.class)
    public void shouldFailWhenKeyIdIsNotDefined() throws Exception {
        KeyProvider<PrivateKey> keyProvider = SystemPropertyKeyProvider.fromUri(VALID_URL, keyReader);
        keyProvider.getKey(ValidatedKeyId.validate("some/other/key/identifier"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailToConstructIfUriDoesNotHaveSyspropSchema() throws Exception {
        SystemPropertyKeyProvider.fromUri(new URI("sysprop:opaque"), keyReader);
    }
}
