package com.atlassian.asap.core.validator;

import com.atlassian.asap.core.exception.InvalidHeaderException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ValidatedKeyIdTest {
    private static final String VALID_KEY_ID = "some/issuer/has/a/key";

    @Test
    public void shouldAcceptValidKeyId() throws Exception {
        ValidatedKeyId.validate(VALID_KEY_ID);
    }

    @Test(expected = InvalidHeaderException.class)
    public void shouldRejectBlankKid() throws Exception {
        ValidatedKeyId.validate(" ");
    }

    @Test(expected = InvalidHeaderException.class)
    public void shouldRejectKidWithDirectoryTraversalComponents() throws Exception {
        ValidatedKeyId.validate("dir1/../dir2");
    }

    @Test(expected = InvalidHeaderException.class)
    public void shouldRejectKidWithLeadingSlash() throws Exception {
        ValidatedKeyId.validate("/dir1/dir2");
    }

    @Test(expected = InvalidHeaderException.class)
    public void shouldRejectKidWithUrlEscapedChars() throws Exception {
        ValidatedKeyId.validate("dir/some%thing");
    }

    @Test(expected = InvalidHeaderException.class)
    public void shouldRejectKidWithNullChar() throws Exception {
        ValidatedKeyId.validate("dir/some\u0000thing");
    }

    @Test(expected = InvalidHeaderException.class)
    public void shouldRejectKidWithQuestionMark() throws Exception {
        ValidatedKeyId.validate("dir/some?thing");
    }

    @Test(expected = InvalidHeaderException.class)
    public void shouldRejectKidWithFragmentSeparator() throws Exception {
        ValidatedKeyId.validate("dir/some#thing");
    }

    @Test(expected = InvalidHeaderException.class)
    public void shouldRejectKidLeadingDoubleSlash() throws Exception {
        ValidatedKeyId.validate("//bar");
    }

    @Test(expected = InvalidHeaderException.class)
    public void shouldRejectKidDoubleSlash() throws Exception {
        ValidatedKeyId.validate("foo//bar/baz");
    }

    @Test(expected = InvalidHeaderException.class)
    public void shouldRejectKidBackSlash() throws Exception {
        ValidatedKeyId.validate("\\foo/bar");
    }

    @Test(expected = InvalidHeaderException.class)
    public void shouldRejectKidBackSlashWithTraversal() throws Exception {
        ValidatedKeyId.validate("foo\\..\\bar");
    }
}
