package com.atlassian.asap.core.keys;

import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import org.junit.Test;

import java.io.Reader;
import java.io.StringReader;
import java.math.BigInteger;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertEquals;

public class PemReaderTest {
    public static final String RSA_PRIVATE_KEY = "-----BEGIN RSA PRIVATE KEY-----\n" +
            "MIICXgIBAAKBgQDPpV0EjiFQ0ZEJ3m7Nyz+pmY+t2t2aqUe/uqW6PLvePe7b9/I0\n" +
            "7Znz65koGidfmsZxy88waj1GO0y9nGwvZ5yicE8dfDId2GruMzgcSWJCmgJ/2/OH\n" +
            "53KaYd9AXLI1ynl5rjwWy8KPrX8XhpTrlEo+Pu57Kc/LHlXL9ubs1sW6ZQIDAQAB\n" +
            "AoGBAJJdyfeQCEPjtQzz0b8WacWvDOxLvrFqabzoYDGq5fJ+TYSYfg54/XBGvira\n" +
            "ZK6rdv5335ANEywSWMG/JTM1Id7JVDI6/Mdbp2Wzu8slNcXUMqS9jb6WY/KlVrdq\n" +
            "ZRhlinTjachnNtK9K2O3EdSxQb9aLwHNboOnu7qPvsBP+p3xAkEA8T7CP0KPpjKo\n" +
            "adrfRo6RI1znivny3+oPPF/0ZnQ1h5vUCKVlRD899w0T71CyS2tIbPbgJ2UTFZ/2\n" +
            "TjljVi3D0wJBANxYiF+WXe/+UnLLydnWDFW6I1fPgm5ZbySluqejIYS02O34KQXj\n" +
            "VVNqif6TW/U+5KQ/Oayb4MNxkkxNmbw0fecCQQDaB1xLC/8Dt7jZooQ0Ilkt2qMw\n" +
            "yWEl2UXXzOj3R4OxgbYJ8mEpYva/tsQTf50D6HvWbvB66jBrVNyoKdmLY2UdAkAs\n" +
            "vl2S63nPzhj37qHidjCzB8U9g4m81rRXAMBSYjHgPMkAKbBK3crp0WyMIWg++LJ9\n" +
            "F8miX1TY2ysWC3v4V8BpAkEAzwUaRygWfprUbYZLiTcqO3MaUEKvBnf+WswUqxkP\n" +
            "jPu20i22yqAHpkqagruNMz9RHS4WCrYLVXHC0+lb1HlGQg==\n" +
            "-----END RSA PRIVATE KEY-----";

    // generated with "openssl ecparam -name secp256k1 -genkey -noout"
    public static final String EC_PRIVATE_KEY = "-----BEGIN EC PRIVATE KEY-----\n" +
            "MHQCAQEEIOcCNQVWSNrUzvqkPQChzSm4ndoN2T0fr7YIh2+5uqgQoAcGBSuBBAAK\n" +
            "oUQDQgAEE9qA2C6bKD+9h6qtuTwYM2yDWI35J6VYK4cTtTE/9rvhvvHuGp8moqeW\n" +
            "dHBRsG2MdblU5+HXrH8FwOHc299UVQ==\n" +
            "-----END EC PRIVATE KEY-----";

    public static final String RSA_PUBLIC_KEY = "-----BEGIN PUBLIC KEY-----\n" +
            "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDPpV0EjiFQ0ZEJ3m7Nyz+pmY+t\n" +
            "2t2aqUe/uqW6PLvePe7b9/I07Znz65koGidfmsZxy88waj1GO0y9nGwvZ5yicE8d\n" +
            "fDId2GruMzgcSWJCmgJ/2/OH53KaYd9AXLI1ynl5rjwWy8KPrX8XhpTrlEo+Pu57\n" +
            "Kc/LHlXL9ubs1sW6ZQIDAQAB\n" +
            "-----END PUBLIC KEY-----";

    // generated from the private key using "openssl ec -in $PRIVATE_KEY -pubout -out $PUBLIC_KEY"
    public static final String EC_PUBLIC_KEY = "-----BEGIN PUBLIC KEY-----\n" +
            "MFYwEAYHKoZIzj0CAQYFK4EEAAoDQgAEE9qA2C6bKD+9h6qtuTwYM2yDWI35J6VY\n" +
            "K4cTtTE/9rvhvvHuGp8moqeWdHBRsG2MdblU5+HXrH8FwOHc299UVQ==\n" +
            "-----END PUBLIC KEY-----";

    public static final String UNPARSEABLE_RSA_KEY = "-----BEGIN RSA PRIVATE KEY-----\n" +
            "MIICXgIBAAKBgQDPpV0EjiFQ0ZEJ3m7Nyz+pmY+t2t2aqUe/uqW6PLvePe7b9/I0\n" +
            "7Znz65koGidfmsZxy88waj1GO0y9nGwvZ5yicE8dfDId2GruMzgcSWJCmgJ/2/OH\n" +
            "53KaYd9AXLI1ynl5rjwWy8KPrX8XhpTrlEo+Pu57Kc/LHlXL9ubs1sW6ZQIDAQAB\n" +
            "AoGBAJJdyfeQCEPjtQzz0b8WacWvDOxLvrFqabzoYDGq5fJ+TYSYfg54/XBGvira\n" +
            "THIS IS AN ERRONEOUS FILE\n" +                                           // <-- error
            "adrfRo6RI1znivny3+oPPF/0ZnQ1h5vUCKVlRD899w0T71CyS2tIbPbgJ2UTFZ/2\n" +
            "TjljVi3D0wJBANxYiF+WXe/+UnLLydnWDFW6I1fPgm5ZbySluqejIYS02O34KQXj\n" +
            "VVNqif6TW/U+5KQ/Oayb4MNxkkxNmbw0fecCQQDaB1xLC/8Dt7jZooQ0Ilkt2qMw\n" +
            "yWEl2UXXzOj3R4OxgbYJ8mEpYva/tsQTf50D6HvWbvB66jBrVNyoKdmLY2UdAkAs\n" +
            "vl2S63nPzhj37qHidjCzB8U9g4m81rRXAMBSYjHgPMkAKbBK3crp0WyMIWg++LJ9\n" +
            "F8miX1TY2ysWC3v4V8BpAkEAzwUaRygWfprUbYZLiTcqO3MaUEKvBnf+WswUqxkP\n" +
            "jPu20i22yqAHpkqagruNMz9RHS4WCrYLVXHC0+lb1HlGQg==\n" +
            "-----END RSA PRIVATE KEY-----";

    private PemReader pemReader = new PemReader();

    @Test
    public void shouldReadRsaPrivateKey() throws Exception {
        Reader reader = new StringReader(RSA_PRIVATE_KEY);
        RSAPrivateKey privateKey = (RSAPrivateKey) pemReader.readPrivateKey(reader);

        assertEquals("PKCS#8", privateKey.getFormat());
        assertEquals("RSA", privateKey.getAlgorithm());

        // we got this expected hex string by running "openssl rsa -text -in <private-key-pem-file-name>", and formatting the 'private exponent' hex output by stripping out the ':' characters
        String expectedPrivateExponentHexString = "00925dc9f7900843e3b50cf3d1bf1669c5af0cec4bbeb16a69bce86031aae5f27e4d84987e0e78fd7046be2ada64aeab76fe77df900d132c1258c1bf25333521dec954323afcc75ba765b3bbcb2535c5d432a4bd8dbe9663f2a556b76a6518658a74e369c86736d2bd2b63b711d4b141bf5a2f01cd6e83a7bbba8fbec04ffa9df1";
        BigInteger expectedPrivateExponent = new BigInteger(expectedPrivateExponentHexString, 16);

        assertEquals(expectedPrivateExponent, privateKey.getPrivateExponent());
    }

    @Test
    public void shouldReadRsaPublicKey() throws Exception {
        Reader reader = new StringReader(RSA_PUBLIC_KEY);
        RSAPublicKey publicKey = (RSAPublicKey) pemReader.readPublicKey(reader);

        // we got this expected hex string by running "openssl rsa -text -pubin -in <public-key-pem-file-name>", and formatting the 'modulus' hex output by stripping out the ':' characters
        String expectedModulusHexString =
                "00cfa55d048e2150d19109de6ecdcb" +
                "3fa9998faddadd9aa947bfbaa5ba3c" +
                "bbde3deedbf7f234ed99f3eb99281a" +
                "275f9ac671cbcf306a3d463b4cbd9c" +
                "6c2f679ca2704f1d7c321dd86aee33" +
                "381c4962429a027fdbf387e7729a61" +
                "df405cb235ca7979ae3c16cbc28fad" +
                "7f178694eb944a3e3eee7b29cfcb1e" +
                "55cbf6e6ecd6c5ba65";

        BigInteger expectedModulus = new BigInteger(expectedModulusHexString, 16);

        // also from "openssl rsa -text -pubin -in <public-key-pem-file-name>"
        BigInteger expectedPublicExponent = new BigInteger("65537");

        assertEquals(expectedModulus, publicKey.getModulus());
        assertEquals(expectedPublicExponent, publicKey.getPublicExponent());

    }

    @Test
    public void shouldReadEcPrivateKey() throws Exception {
        Reader reader = new StringReader(EC_PRIVATE_KEY);
        ECPrivateKey privateKey = (ECPrivateKey) pemReader.readPrivateKey(reader);

        assertEquals("PKCS#8", privateKey.getFormat());
        assertEquals("ECDSA", privateKey.getAlgorithm());

        // we got this expected hex string by running "openssl ec -text -in $PRIVATE_KEY", and formatting the 'priv' hex output by stripping out the ':' characters
        String expectedPrivateKeyHexString =
                "00e70235055648dad4cefaa43d00a1" +
                "cd29b89dda0dd93d1fafb608876fb9" +
                "baa810";
        BigInteger expectedPrivateKey = new BigInteger(expectedPrivateKeyHexString, 16);

        assertEquals(expectedPrivateKey, privateKey.getS());
    }

    @Test
    public void shouldReadEcPublicKey() throws Exception {
        Reader reader = new StringReader(EC_PUBLIC_KEY);

        assertThat(pemReader.readPublicKey(reader), instanceOf(ECPublicKey.class));
    }

    @Test(expected = CannotRetrieveKeyException.class)
    public void shouldFailToReadBadPrivateKey() throws Exception {
        Reader reader = new StringReader(UNPARSEABLE_RSA_KEY);
        pemReader.readPrivateKey(reader);
    }
}
