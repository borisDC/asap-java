package com.atlassian.asap.core;

/**
 * Constants defined by the ASAP protocol.
 */
public final class JwtConstants {
    /**
     * The WWW-Authenticate header value for the bearer authentication scheme.
     */
    public static final String BEARER_AUTHENTICATION_SCHEME = "Bearer";

    /**
     * The Authorization HTTP header value prefix for the bearer authentication scheme.
     */
    public static final String HTTP_AUTHORIZATION_HEADER_VALUE_PREFIX = BEARER_AUTHENTICATION_SCHEME + " ";

    private JwtConstants() {
        throw new UnsupportedOperationException();
    }
}
