package com.atlassian.asap.core.exception;

import com.atlassian.asap.api.exception.InvalidTokenException;

/**
 * Thrown if an incoming token is signed using an unsupported algorithm.
 */
public class UnsupportedAlgorithmException extends InvalidTokenException {
    public UnsupportedAlgorithmException(String message) {
        super(message);
    }

    public UnsupportedAlgorithmException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnsupportedAlgorithmException(Throwable cause) {
        super(cause);
    }
}
