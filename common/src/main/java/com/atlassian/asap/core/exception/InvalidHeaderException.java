package com.atlassian.asap.core.exception;

import com.atlassian.asap.api.exception.InvalidTokenException;

/**
 * Thrown if a JWT header does not match its expected format.
 */
public class InvalidHeaderException extends InvalidTokenException {
    public InvalidHeaderException(String message) {
        super(message);
    }

    public InvalidHeaderException(String message, Throwable cause) {
        super(message, cause);
    }
}
