package com.atlassian.asap.core.keys;

import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.core.exception.PublicKeyNotFoundException;
import com.atlassian.asap.core.validator.ValidatedKeyId;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.Key;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;

/**
 * <p>Looks up keys sequentially in a given list of key providers.</p>
 *
 * <p>It will throw {@link PublicKeyNotFoundException} if all the providers are
 * available and none of them contains the key. It will throw
 * {@link CannotRetrieveKeyException} if at least one of the providers is down
 * and the key cannot be found in any of the others.</p>
 *
 * <p>Note that the current implementation doesn't perform any caching, in
 * particular it doesn't remember where the key was found last time. That may
 * reduce the effectiveness of the provider's http cache if it comes later
 * in the keyProviderChain.</p>
 *
 * @param <K> the type of key retrieved by this chained key provider
 */
public final class ChainedKeyProvider<K extends Key> implements KeyProvider<K> {
    private static final Logger logger = LoggerFactory.getLogger(com.atlassian.asap.core.keys.publickey.ChainedKeyProvider.class);


    private final List<KeyProvider<K>> keyProviderChain;
    private final Function<ValidatedKeyId, ? extends CannotRetrieveKeyException> exceptionBuilder;

    public ChainedKeyProvider(List<KeyProvider<K>> keyProviderChain,
                              Function<ValidatedKeyId, ? extends CannotRetrieveKeyException> exceptionBuilder) {
        this.keyProviderChain = ImmutableList.copyOf(keyProviderChain);
        this.exceptionBuilder = requireNonNull(exceptionBuilder);
    }

    /**
     * Create a chained key provider representing a given list of key repository providers.
     * If there is only one provider in the chain, then return the provider instead of wrapping it.
     *
     * @param keyProviderChain set of chained key repository providers in order
     * @param <K>              the type of key retrieved by this chained key provider
     * @return the chained key provider if there are more than one providers, or a key provider when it is
     * the only provider in the chain
     */
    public static <K extends Key> KeyProvider<K> createChainedKeyProvider(List<KeyProvider<K>> keyProviderChain) {
        // this is an optimization to avoid wrapping the key provider
        // when there is only one keyprovider
        if (keyProviderChain.size() == 1) {
            return keyProviderChain.get(0);
        } else {
            return new ChainedKeyProvider<>(keyProviderChain,
                    keyId -> new CannotRetrieveKeyException(format("None of the chained key providers contains the key: %s", keyId.getKeyId())));
        }
    }

    @Override
    public K getKey(ValidatedKeyId validatedKeyId) throws CannotRetrieveKeyException {
        if (keyProviderChain.isEmpty()) {
            throw new CannotRetrieveKeyException("There are no key providers available in the chain");
        }
        Iterator<KeyProvider<K>> keyProviderIterator = keyProviderChain.iterator();
        CannotRetrieveKeyException firstError = null;
        List<KeyProvider<K>> failingProviders = Lists.newLinkedList();
        while (keyProviderIterator.hasNext()) {
            KeyProvider<K> keyProvider = keyProviderIterator.next();
            try {
                return keyProvider.getKey(validatedKeyId);
            } catch (PublicKeyNotFoundException ex) {
                logger.debug("Key not found in the provider {}, continuing with the next provider in the chain", keyProvider);
            } catch (CannotRetrieveKeyException ex) {
                failingProviders.add(keyProvider);
                // remember the first error
                if (firstError == null) {
                    firstError = ex;
                }
                logger.debug("Error retrieving key from the provider {}, continuing with the next provider in the chain", keyProvider, ex);
            }
        }

        // iteration finished without finding the key
        if (firstError != null) {
            logger.warn("Unable to retrieve key from chained key providers: {}. Showing first error.", failingProviders, firstError);
            throw new CannotRetrieveKeyException("Unable to retrieve key from chained key providers. " + failingProviders.size() +
                    " provider(s) could not be contacted. Chaining first error.", firstError);
        } else {
            logger.debug("None of the chained key providers contains the key: {}", keyProviderChain);
            throw exceptionBuilder.apply(validatedKeyId);
        }
    }

    /**
     * @return the chain of key providers in the same order as they are consulted to resolve a key
     */
    public List<KeyProvider<K>> getKeyProviderChain() {
        return keyProviderChain;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{" + keyProviderChain + '}';
    }
}

