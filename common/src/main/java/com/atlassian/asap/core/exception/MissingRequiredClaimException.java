package com.atlassian.asap.core.exception;

import com.atlassian.asap.api.JwtClaims;

/**
 * Thrown when a required claim is missing in the JWT claims payload.
 */
public class MissingRequiredClaimException extends JwtParseException {
    private final JwtClaims.RegisteredClaim claim;

    public MissingRequiredClaimException(JwtClaims.RegisteredClaim claim) {
        super("JWT token missing required claim: " + claim.key());
        this.claim = claim;
    }

    public JwtClaims.RegisteredClaim getClaim() {
        return claim;
    }

    @Override
    public String getSafeDetails() {
        return super.getSafeDetails() + " - " + claim;
    }
}
