package com.atlassian.asap.core.exception;

import com.atlassian.asap.api.exception.InvalidTokenException;

import java.time.Instant;

/**
 * Thrown if the token timestamps show that it has expired.
 */
public class TokenExpiredException extends InvalidTokenException {
    public TokenExpiredException(Instant expiredAt, Instant now) {
        super(String.format("Expired at %s and time is now %s", expiredAt, now));
    }
}
