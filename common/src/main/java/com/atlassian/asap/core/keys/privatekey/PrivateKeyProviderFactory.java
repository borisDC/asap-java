package com.atlassian.asap.core.keys.privatekey;

import com.atlassian.asap.core.keys.DataUriKeyReader;
import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.keys.PemReader;
import com.atlassian.asap.core.keys.privatekey.EnvironmentVariableKeyProvider.Environment;
import com.google.common.annotations.VisibleForTesting;

import java.io.File;
import java.net.URI;
import java.security.PrivateKey;

public class PrivateKeyProviderFactory {
    /**
     * Creates an instance of {@link com.atlassian.asap.core.keys.KeyProvider}.
     *
     * @param privateKeyPath the base url to use for retrieving private keys
     * @return a private key provider
     * @throws IllegalArgumentException if the privateKeyPath is syntactically incorrect
     */
    public static KeyProvider<PrivateKey> createPrivateKeyProvider(URI privateKeyPath) {
        return createPrivateKeyProvider(privateKeyPath, new Environment());
    }

    @VisibleForTesting
    static KeyProvider<PrivateKey> createPrivateKeyProvider(URI privateKeyPath, Environment environment) {
        switch (privateKeyPath.getScheme()) {
            case "file":
                return new FilePrivateKeyProvider(new File(privateKeyPath), new PemReader());
            case "classpath":
                return new ClasspathPrivateKeyProvider(privateKeyPath.getPath(), new PemReader());
            case "data":
                return new DataUriKeyProvider(privateKeyPath, new DataUriKeyReader());
            case SystemPropertyKeyProvider.URL_SCHEME:
                return SystemPropertyKeyProvider.fromUri(privateKeyPath, new DataUriKeyReader());
            case EnvironmentVariableKeyProvider.URL_SCHEME:
                return EnvironmentVariableKeyProvider.fromUri(privateKeyPath, new DataUriKeyReader(), environment);
            case NullKeyProvider.URL_SCHEME:
                return new NullKeyProvider();
            default:
                throw new IllegalArgumentException("Unsupported private key base URL: " + privateKeyPath);
        }
    }
}
