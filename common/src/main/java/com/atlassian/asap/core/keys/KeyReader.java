package com.atlassian.asap.core.keys;

import com.atlassian.asap.api.exception.CannotRetrieveKeyException;

import java.io.Reader;
import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * Reads cryptographic keys from encoded key data.
 */
public interface KeyReader {
    PrivateKey readPrivateKey(Reader keyData) throws CannotRetrieveKeyException;

    PublicKey readPublicKey(Reader reader) throws CannotRetrieveKeyException;
}
