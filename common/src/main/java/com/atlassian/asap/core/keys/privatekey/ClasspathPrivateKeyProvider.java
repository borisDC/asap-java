package com.atlassian.asap.core.keys.privatekey;

import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.keys.KeyReader;
import com.atlassian.asap.core.validator.ValidatedKeyId;
import com.google.common.base.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.security.PrivateKey;
import java.util.Objects;

import static com.atlassian.asap.core.keys.ClassPathUri.classPathUri;

/**
 * Retrieves private keys from the classpath.
 */
public class ClasspathPrivateKeyProvider implements KeyProvider<PrivateKey> {
    private static final Logger logger = LoggerFactory.getLogger(ClasspathPrivateKeyProvider.class);

    private final KeyReader keyReader;
    private final String classpathBase;

    /**
     * Create instance of ClasspathPrivateKeyProvider that reads private keys under the given classpath base.
     */
    public ClasspathPrivateKeyProvider(String classpathBase, KeyReader keyReader) {
        this.keyReader = Objects.requireNonNull(keyReader);
        this.classpathBase = Objects.requireNonNull(classpathBase);
        Preconditions.checkArgument(classpathBase.endsWith("/"), "Classpath prefix must end with a slash");
    }

    @Override
    public PrivateKey getKey(ValidatedKeyId validatedKeyId) throws CannotRetrieveKeyException {
        String pathToPrivateKey = classpathBase + validatedKeyId.getKeyId();

        logger.debug("Reading private key from classpath: {}", pathToPrivateKey);

        try (InputStream inputStream = ClasspathPrivateKeyProvider.class.getResourceAsStream(pathToPrivateKey)) {
            if (inputStream == null) {
                throw new CannotRetrieveKeyException("Private key not found in the classpath", classPathUri(pathToPrivateKey));
            }
            try (InputStreamReader reader = new InputStreamReader(inputStream, StandardCharsets.US_ASCII)) {
                return keyReader.readPrivateKey(reader);
            }
        } catch (IOException e) {
            throw new CannotRetrieveKeyException("Error retrieving private key from classpath", e, classPathUri(pathToPrivateKey));
        }
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
