package com.atlassian.asap.api.exception;

import com.atlassian.asap.core.validator.ValidatedKeyId;

import javax.annotation.Nullable;
import java.net.URI;
import java.util.Optional;

/**
 * Thrown when an incoming HTTP request cannot be authenticated using ASAP.
 */
public abstract class AuthenticationFailedException extends Exception {
    /**
     * The validated key ID that was used when the authentication failure happened. May be {@code null}
     */
    @Nullable
    private final ValidatedKeyId keyId;

    /**
     * The URI from which the key was fetched, if it was.
     */
    @Nullable
    private final URI keyUri;

    /**
     * The unverified issuer that used when the authentication failure happened. May be {@code null}
     */
    @Nullable
    private final String unverifiedIssuer;

    protected AuthenticationFailedException(String message, String unverifiedIssuer) {
        this(message, null, null, unverifiedIssuer);
    }

    protected AuthenticationFailedException(String message, ValidatedKeyId keyId, URI keyUri, String unverifiedIssuer) {
        super(message);
        this.keyId = keyId;
        this.keyUri = keyUri;
        this.unverifiedIssuer = unverifiedIssuer;
    }

    public Optional<ValidatedKeyId> getKeyId() {
        return Optional.ofNullable(keyId);
    }

    public Optional<URI> getKeyUri() {
        return Optional.ofNullable(keyUri);
    }

    public Optional<String> getUnverifiedIssuer() {
        return Optional.ofNullable(unverifiedIssuer);
    }
}
