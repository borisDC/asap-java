package com.atlassian.asap.service.core.impl;

import com.atlassian.asap.service.api.TokenValidator;
import com.atlassian.asap.service.core.spi.AsapConfiguration;
import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableSet;

import java.util.Set;

import static java.util.Objects.requireNonNull;

/**
 * Implements the builder logic for a token validator.
 */
@SuppressWarnings("WeakerAccess")
public abstract class AbstractTokenValidator implements TokenValidator {
    private final AsapConfiguration config;

    /**
     * The issuers that are <strong>authorized</strong> to use this resource as 'root'.
     */
    private Set<String> authorizedIssuers;

    /**
     * The issuers that are <strong>authorized</strong> to use this resource.
     */
    private Set<String> impersonationAuthorizedIssuers;

    /**
     * The effective subjects that are <strong>authorized</strong> to use this resource.
     */
    private Set<String> authorizedSubjects;

    /**
     * The audience values that may be specified when creating a token for this resource.
     */
    private Set<String> acceptableAudienceValues;

    /**
     * True if the token's subject value is going to be interpreted as a user to impersonate.
     *
     * @see TokenValidator#subject(String...)
     */
    private boolean subjectImpersonation;

    /**
     * The validation policy to apply when validating the token.
     */
    private Policy policy = Policy.REQUIRE;


    @SuppressWarnings("WeakerAccess")
    protected AbstractTokenValidator(AsapConfiguration config) {
        this.config = requireNonNull(config, "config");
        this.authorizedIssuers = ImmutableSet.of();
        this.impersonationAuthorizedIssuers = ImmutableSet.of();
        this.authorizedSubjects = ImmutableSet.of();
        this.acceptableAudienceValues = ImmutableSet.of(config.audience());
    }

    @Override
    public TokenValidator issuer(Iterable<String> authorizedIssuers) {
        this.authorizedIssuers = ImmutableSet.copyOf(authorizedIssuers);
        return this;
    }

    @Override
    public TokenValidator impersonationIssuer(Iterable<String> impersonationIssuers) {
        this.impersonationAuthorizedIssuers = ImmutableSet.copyOf(impersonationIssuers);
        return this;
    }

    /**
     * @param subjectImpersonation {@code true} to use subject impersonation
     * @return {@code this}
     * @deprecated move/copy issuers that are allowed to impersonate users from the 'issuer' to the 'impersonationIssuer' list
     */
    @Override
    @Deprecated
    public TokenValidator subjectImpersonation(boolean subjectImpersonation) {
        this.subjectImpersonation = subjectImpersonation;
        return this;
    }

    @Override
    public TokenValidator subject(Iterable<String> authorizedSubjects) {
        this.authorizedSubjects = ImmutableSet.copyOf(authorizedSubjects);
        return this;
    }

    @Override
    public TokenValidator audience(Iterable<String> additionalAudienceValues) {
        this.acceptableAudienceValues = ImmutableSet.<String>builder()
                .add(config.audience())
                .addAll(additionalAudienceValues)
                .build();
        return this;
    }

    @Override
    public TokenValidator policy(Policy policy) {
        this.policy = requireNonNull(policy, "policy");
        return this;
    }

    protected Set<String> authorizedIssuers() {
        return authorizedIssuers;
    }

    protected Set<String> impersonationAuthorizedIssuers() {
        return impersonationAuthorizedIssuers;
    }

    protected Set<String> authorizedSubjects() {
        return authorizedSubjects;
    }

    protected Set<String> acceptableAudienceValues() {
        return acceptableAudienceValues;
    }

    protected boolean subjectImpersonation() {
        return subjectImpersonation;
    }

    protected Policy policy() {
        return policy;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("config", config)
                .add("authorizedIssuers", authorizedIssuers)
                .add("impersonationAuthorizedIssuers", impersonationAuthorizedIssuers)
                .add("authorizedSubjects", authorizedSubjects)
                .add("acceptableAudienceValues", acceptableAudienceValues)
                .add("subjectImpersonation", subjectImpersonation)
                .add("policy", policy)
                .toString();
    }
}
