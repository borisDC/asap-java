package com.atlassian.asap.service.core.service;

import com.google.common.annotations.VisibleForTesting;

import java.util.Optional;
import java.util.function.Function;

@SuppressWarnings("WeakerAccess")
public class EnvVarAsapConfiguration extends StringValuesAsapConfiguration {
    // These are the environment variables that Micros uses when a service descriptor includes "requiresAsap: true".
    static final String ASAP_ISSUER = "ASAP_ISSUER";
    static final String ASAP_KEY_ID = "ASAP_KEY_ID";
    static final String ASAP_AUDIENCE = "ASAP_AUDIENCE";
    static final String ASAP_PUBLIC_KEY_REPOSITORY_URL = "ASAP_PUBLIC_KEY_REPOSITORY_URL";
    static final String ASAP_PRIVATE_KEY = "ASAP_PRIVATE_KEY";

    public EnvVarAsapConfiguration(String defaultIssuer, String defaultKeyId, String defaultAudience) {
        this(defaultIssuer, defaultKeyId, defaultAudience, varName -> Optional.ofNullable(System.getenv(varName)));
    }

    @VisibleForTesting
    EnvVarAsapConfiguration(String defaultIssuer, String defaultKeyId, String defaultAudience,
                            Function<String, Optional<String>> getenv) {
        super(getenv.apply(ASAP_ISSUER).orElse(defaultIssuer),
                getenv.apply(ASAP_KEY_ID).orElse(defaultKeyId),
                getenv.apply(ASAP_AUDIENCE).orElse(defaultAudience),
                getenv.apply(ASAP_PUBLIC_KEY_REPOSITORY_URL).orElse(""),
                getenv.apply(ASAP_PRIVATE_KEY).orElse(""));
    }
}

