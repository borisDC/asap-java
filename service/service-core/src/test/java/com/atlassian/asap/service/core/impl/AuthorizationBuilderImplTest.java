package com.atlassian.asap.service.core.impl;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.client.http.AuthorizationHeaderGenerator;
import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.api.exception.InvalidTokenException;
import com.atlassian.asap.core.exception.MissingRequiredClaimException;
import com.atlassian.asap.service.core.spi.AsapConfiguration;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonValue;
import java.time.Duration;
import java.util.Optional;

import static com.atlassian.asap.service.core.impl.JwtMatcher.jwtMatcher;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@SuppressWarnings("NullableProblems")
public class AuthorizationBuilderImplTest {
    private static final String ISSUER = "harry";
    private static final String KEY_ID = "harry/key";
    private static final String AUDIENCE1 = "hogwarts";
    private static final String AUDIENCE2 = "dursleys";
    private static final String SUBJECT = "ron";
    private static final String TOKEN = "token";

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();
    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Mock
    private AsapConfiguration config;
    @Mock
    private AuthorizationHeaderGenerator generator;
    @Captor
    private ArgumentCaptor<Jwt> jwtCaptor;

    @InjectMocks
    private AuthorizationBuilderImpl builder;

    @Before
    public void setUp() throws CannotRetrieveKeyException, InvalidTokenException {
        when(config.issuer()).thenReturn(ISSUER);
        when(config.keyId()).thenReturn(KEY_ID);
        when(config.audience()).thenReturn(AUDIENCE1);
        when(generator.generateAuthorizationHeader(jwtCaptor.capture())).thenReturn(TOKEN);
    }

    @Test
    public void audienceMayNotBeEmpty() throws Exception {
        thrown.expect(MissingRequiredClaimException.class);
        thrown.expectMessage(endsWith(": aud"));
        builder.build();
    }

    @Test
    public void defaultsWithSingleAudience() throws Exception {
        assertThat(builder.audience(AUDIENCE1).build(), is(TOKEN));
        assertThat(jwtCaptor.getValue(), jwtMatcher()
                .issuer(ISSUER)
                .keyId(KEY_ID)
                .subject(Optional.empty())
                .audience(AUDIENCE1));
    }

    @Test
    public void withModifiedValues() throws Exception {
        final Duration duration = Duration.ofSeconds(42L);

        assertThat(builder.audience(AUDIENCE1, AUDIENCE2)
                .subject(Optional.of(SUBJECT))
                .expiration(Optional.of(duration))
                .build(), is(TOKEN));

        assertThat(jwtCaptor.getValue(), jwtMatcher()
                .issuer(ISSUER)
                .keyId(KEY_ID)
                .subject(Optional.of(SUBJECT))
                .expiration(duration)
                .audience(AUDIENCE1, AUDIENCE2));
    }

    @Test
    public void withCustomClaims() throws Exception {
        final JsonObject nestedObject = Json.createObjectBuilder()
                .add("nifty", JsonValue.NULL)
                .build();
        final JsonObject customClaims = Json.createObjectBuilder()
                .add("hello", "world")
                .add("number", 42)
                .add("nested", nestedObject)
                .build();

        assertThat(builder.audience(AUDIENCE1)
                .customClaims(Optional.of(customClaims))
                .build(), is(TOKEN));
        assertThat(jwtCaptor.getValue(), jwtMatcher()
                .issuer(ISSUER)
                .keyId(KEY_ID)
                .customClaim("hello", customClaims.getJsonString("hello"))
                .customClaim("number", customClaims.getJsonNumber("number"))
                .customClaim("nested", nestedObject));
    }
}
