package com.atlassian.asap.service.core.service;

import com.atlassian.asap.api.client.http.AuthorizationHeaderGenerator;
import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.validator.JwtClaimsValidator;
import com.atlassian.asap.service.api.AsapAuth;
import com.atlassian.asap.service.api.TokenValidator;
import com.atlassian.asap.service.api.ValidationResult;
import com.atlassian.asap.service.core.spi.AsapConfiguration;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.lang.reflect.Method;
import java.security.PublicKey;
import java.util.Optional;

import static com.atlassian.asap.service.api.TokenValidator.Policy.OPTIONAL;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SuppressWarnings("NullableProblems")
public class DefaultAsapServiceTest {
    private static final String ISSUER1 = "ron";
    private static final String ISSUER2 = "hermione";
    private static final String KEY_ID = "ron/key1";
    private static final String AUDIENCE1 = "harry";
    private static final String AUDIENCE2 = "george";
    private static final String TOKEN = "token";
    private static final Optional<String> HEADER = Optional.of("Bearer " + TOKEN);

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();
    @Rule
    public final TestName testName = new TestName();

    @Mock
    private AsapConfiguration config;
    @Mock
    private JwtClaimsValidator jwtClaimsValidator;
    @Mock
    private KeyProvider<PublicKey> publicKeyProvider;
    @Mock
    private AuthorizationHeaderGenerator authorizationHeaderGenerator;
    @Mock
    private ValidationResult validationResult;

    private TokenValidator tokenValidator;
    private DefaultAsapService asapService;

    @Before
    public void setUp() {
        when(config.issuer()).thenReturn(ISSUER1);
        when(config.keyId()).thenReturn(KEY_ID);
        when(config.audience()).thenReturn(AUDIENCE1);

        this.asapService = new DefaultAsapService(config, jwtClaimsValidator, publicKeyProvider, authorizationHeaderGenerator) {
            @Override
            public TokenValidator tokenValidator() {
                tokenValidator = spy(super.tokenValidator());
                doReturn(validationResult).when(tokenValidator).validate(HEADER);
                return tokenValidator;
            }
        };
    }

    @AsapAuth
    @Test
    public void defaultAnnotationSettingsAreAppliedToValidator() {
        callValidate();

        verify(tokenValidator).issuer();
        verify(tokenValidator).impersonationIssuer();
        verify(tokenValidator).subject();
        //remove as soon as deprecated com.atlassian.asap.service.api.AsapAuth.subjectImpersonation is removed
        verify(tokenValidator).subjectImpersonation(false);
        verify(tokenValidator).audience();
        verify(tokenValidator).policy(TokenValidator.Policy.REQUIRE);
    }

    @AsapAuth(issuer = {ISSUER1, ISSUER2},
            subject = {ISSUER2, ISSUER1},
            subjectImpersonation = true,
            audience = {AUDIENCE2},
            policy = OPTIONAL)
    @Test
    //remove as soon as deprecated com.atlassian.asap.service.api.AsapAuth.subjectImpersonation is removed
    public void customAnnotationSettingsAreAppliedToValidator() {
        callValidate();

        verify(tokenValidator).issuer(ISSUER1, ISSUER2);
        verify(tokenValidator).subject(ISSUER2, ISSUER1);
        verify(tokenValidator).subjectImpersonation(true);
        verify(tokenValidator).audience(AUDIENCE2);
        verify(tokenValidator).policy(TokenValidator.Policy.OPTIONAL);
    }


    @AsapAuth(issuer = {ISSUER1, ISSUER2},
            subject = {ISSUER2, ISSUER1},
            impersonationIssuer = {ISSUER2},
            audience = {AUDIENCE2},
            policy = OPTIONAL)
    @Test
    public void impersonationAnnotationSettingsAreAppliedToValidator() {
        callValidate();

        verify(tokenValidator).issuer(ISSUER1, ISSUER2);
        verify(tokenValidator).impersonationIssuer(ISSUER2);
        verify(tokenValidator).subject(ISSUER2, ISSUER1);
        verify(tokenValidator).audience(AUDIENCE2);
        verify(tokenValidator).policy(TokenValidator.Policy.OPTIONAL);
    }

    private void callValidate() {
        try {
            final Method method = getClass().getMethod(testName.getMethodName());
            final AsapAuth annotation = method.getAnnotation(AsapAuth.class);
            assertNotNull("Test method wasn't annotated?", annotation);
            assertThat(asapService.validate(annotation, HEADER), sameInstance(validationResult));
        } catch (NoSuchMethodException e) {
            throw new AssertionError(e);
        }
    }
}
