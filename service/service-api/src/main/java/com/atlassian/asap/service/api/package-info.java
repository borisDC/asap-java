/**
 * A service API that simplifies creating and verifying ASAP tokens within an application.
 */
@FieldsAreNonNullByDefault
@ParametersAreNonnullByDefault
@ReturnTypesAreNonNullByDefault

package com.atlassian.asap.service.api;

import com.atlassian.asap.annotation.FieldsAreNonNullByDefault;
import com.atlassian.asap.annotation.ReturnTypesAreNonNullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;
