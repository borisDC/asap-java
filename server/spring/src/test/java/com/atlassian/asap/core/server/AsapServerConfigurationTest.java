package com.atlassian.asap.core.server;

import org.junit.Test;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class AsapServerConfigurationTest {
    @Test
    public void shouldUseAudienceIfOverrideIsNotSpecified() {
        assertThat(new AsapServerConfiguration("abc", "", "").getAllAudiences(), contains("abc"));
    }

    @Test
    public void shouldUseAudienceOverrideIfSpecified() {
        assertThat(new AsapServerConfiguration("abc", "1,2,3", "").getAllAudiences(), containsInAnyOrder("1", "2", "3"));
    }

    @Test
    public void shouldNotUseAdditionalPublicKeyRepositoryIfNotSpecified() {
        assertThat(new AsapServerConfiguration("", "", "").getCombinedPublicKeyRepositoryBaseUrl("main"), is("main"));
    }

    @Test
    public void shouldAddAdditionalPublicKeyRepositoryIfSpecified() {
        assertThat(new AsapServerConfiguration("", "", "additional").getCombinedPublicKeyRepositoryBaseUrl("main"),
                is("main , additional"));
    }
}
