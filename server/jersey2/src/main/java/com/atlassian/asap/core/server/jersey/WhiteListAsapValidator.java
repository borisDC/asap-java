package com.atlassian.asap.core.server.jersey;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.exception.AuthorizationFailedException;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;

/**
 * Validates a valid {@link Jwt} token against a whitelist of acceptable subjects and/or issuers.
 */
@SuppressWarnings("WeakerAccess")
final class WhiteListAsapValidator implements AsapValidator {

    private final Function<Asap, Whitelist> whitelistProvider;

    WhiteListAsapValidator(Function<Asap, Whitelist> whitelistProvider) {
        this.whitelistProvider = requireNonNull(whitelistProvider);
    }

    private static String joinSet(final Set<String> validIssuers) {
        return String.join(",", validIssuers);
    }

    /**
     * Validates a jwt token.
     *
     * @param asap The annotation instance on a method, class, or package
     * @param jwt  The jwt token
     * @throws AuthorizationFailedException If the authorized subjects or issuers fails
     */
    @Override
    public void validate(Asap asap, Jwt jwt) throws AuthorizationFailedException {
        Whitelist whitelist = whitelistProvider.apply(asap);
        validateSubject(whitelist, jwt);
        validateIssuer(whitelist, jwt);
    }

    private void validateIssuer(final Whitelist whitelist, final Jwt jwt)
            throws AuthorizationFailedException {
        String issuer = jwt.getClaims().getIssuer();
        Set<String> validIssuers = !whitelist.authorizedIssuers.isEmpty() ? whitelist.authorizedIssuers :
                whitelist.authorizedSubjects;
        if (!validIssuers.isEmpty() && !validIssuers.contains(issuer)) {
            throw new AuthorizationFailedException(format("Unacceptable issuer ('%s' not in '%s')", issuer,
                    joinSet(validIssuers)));
        }
    }

    private void validateSubject(final Whitelist whitelist, final Jwt jwt) throws AuthorizationFailedException {
        String subject = jwt.getClaims().getSubject().orElse(jwt.getClaims().getIssuer());
        if (!whitelist.authorizedSubjects.isEmpty() &&
                !whitelist.authorizedSubjects.contains(subject)) {
            throw new AuthorizationFailedException(format("Unacceptable subject ('%s' not in '%s')", subject,
                    joinSet(whitelist.authorizedSubjects)));
        }
    }

    /**
     * Provides whitelisted values.  If empty, treat as all allowed.
     */
    @SuppressWarnings("checkstyle:VisiblityModifier")
    static class Whitelist {
        private final Set<String> authorizedSubjects;
        private final Set<String> authorizedIssuers;

        Whitelist(final Set<String> authorizedSubjects, final Set<String> authorizedIssuers) {
            this.authorizedSubjects = Collections.unmodifiableSet(requireNonNull(authorizedSubjects));
            this.authorizedIssuers = Collections.unmodifiableSet(requireNonNull(authorizedIssuers));
        }

        Set<String> getAuthorizedSubjects() {
            return authorizedSubjects;
        }

        Set<String> getAuthorizedIssuers() {
            return authorizedIssuers;
        }
    }

    static class AsapWhitelistProvider implements Function<Asap, Whitelist> {
        @Override
        public Whitelist apply(final Asap asap) {
            return new Whitelist(
                    Arrays.stream(asap.authorizedSubjects()).collect(Collectors.toSet()),
                    Arrays.stream(asap.authorizedIssuers()).collect(Collectors.toSet())
            );
        }
    }

    static class AsapAnnotationWhitelistProviderWithConfigSupport implements Function<Asap, Whitelist> {

        private final Whitelist whiteList;

        AsapAnnotationWhitelistProviderWithConfigSupport(final Set<String> authorizedSubjects, final Set<String> authorizedIssuers) {
            this.whiteList = new Whitelist(authorizedSubjects, authorizedIssuers);

        }

        @Override
        public Whitelist apply(final Asap asap) {

            if (asap.authorizedIssuers().length == 0 && asap.authorizedSubjects().length == 0) {
                return whiteList;
            }
            return new Whitelist(
                    Arrays.stream(asap.authorizedSubjects()).collect(Collectors.toSet()),
                    Arrays.stream(asap.authorizedIssuers()).collect(Collectors.toSet())
            );
        }
    }


    /**
     * Provides the whitelist from environment variables.  The values should be comma-delimited, and an empty or missing
     * value will be treated as allow all.
     */
    static class EnvironmentVariablesWhitelistProvider implements Function<Asap, Whitelist> {
        public static final String AUTHORIZED_SUBJECTS_KEY = "ASAP_AUTHORIZED_SUBJECTS";
        public static final String AUTHORIZED_ISSUERS_KEY = "ASAP_AUTHORIZED_ISSUERS";

        private final String authorizedSubjectsVariableName;
        private final String authorizedIssuersVariableName;
        private final Map<String, String> variables;

        EnvironmentVariablesWhitelistProvider() {
            this(AUTHORIZED_SUBJECTS_KEY, AUTHORIZED_ISSUERS_KEY, System.getenv());
        }

        EnvironmentVariablesWhitelistProvider(final String authorizedSubjectsVariableName,
                                              final String authorizedIssuersVariableName, Map<String, String> variables) {
            this.authorizedSubjectsVariableName = requireNonNull(authorizedSubjectsVariableName);
            this.authorizedIssuersVariableName = requireNonNull(authorizedIssuersVariableName);
            this.variables = variables;
        }

        @Override
        public Whitelist apply(final Asap asap) {
            return new Whitelist(
                    getEnv(authorizedSubjectsVariableName),
                    getEnv(authorizedIssuersVariableName)
            );
        }

        private Set<String> getEnv(String name) {
            return Stream.of(variables.getOrDefault(name, "").split(","))
                    .map(String::trim)
                    .filter(StringUtils::isNotEmpty)
                    .collect(Collectors.toSet());
        }
    }
}
