package com.atlassian.asap.core.server.http;

import com.atlassian.asap.api.JwsHeader;
import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtClaims;
import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.api.exception.PermanentAuthenticationFailedException;
import com.atlassian.asap.api.exception.TransientAuthenticationFailedException;
import com.atlassian.asap.api.server.http.RequestAuthenticator;
import com.atlassian.asap.core.exception.InvalidClaimException;
import com.atlassian.asap.core.exception.MissingRequiredClaimException;
import com.atlassian.asap.core.exception.MissingRequiredHeaderException;
import com.atlassian.asap.core.exception.SignatureMismatchException;
import com.atlassian.asap.core.serializer.JwtSerializer;
import com.atlassian.asap.core.validator.JwtValidator;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RequestAuthenticatorImplTest {
    private static final String SERIALIZED_JWT = "my.jwt.token";

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Mock
    private JwtValidator jwtValidator;
    @Mock
    private JwtSerializer jwtSerializer;
    @Mock
    private Jwt jwt;

    private RequestAuthenticator requestAuthenticator;

    @Before
    public void setUp() throws Exception {
        requestAuthenticator = new RequestAuthenticatorImpl(jwtValidator);
    }

    @Test(expected = PermanentAuthenticationFailedException.class)
    public void shouldFailIfHeaderNotInRightFormat() throws Exception {
        requestAuthenticator.authenticateRequest("a bad authorisation header");
        verifyZeroInteractions(jwtValidator);
    }

    @Test
    public void shouldFailIfValidationFails() throws Exception {
        when(jwtValidator.readAndValidate(SERIALIZED_JWT)).thenThrow(new SignatureMismatchException("Couldn't validate token"));
        when(jwtValidator.determineUnverifiedIssuer(SERIALIZED_JWT)).thenReturn(Optional.of("some-service"));

        expectedException.expect(PermanentAuthenticationFailedException.class);
        expectedException.expectMessage("Failed to authenticate request from some-service (issuer not verified): SignatureMismatchException");

        requestAuthenticator.authenticateRequest("Bearer " + SERIALIZED_JWT);
    }

    @Test
    public void shouldIncludeFailingClaims() throws Exception {
        when(jwtValidator.readAndValidate(SERIALIZED_JWT)).thenThrow(new InvalidClaimException(JwtClaims.RegisteredClaim.EXPIRY, "Token is expired!"));
        when(jwtValidator.determineUnverifiedIssuer(SERIALIZED_JWT)).thenReturn(Optional.of("some-service"));

        expectedException.expect(PermanentAuthenticationFailedException.class);
        expectedException.expectMessage("Failed to authenticate request from some-service (issuer not verified): InvalidClaimException - EXPIRY");

        requestAuthenticator.authenticateRequest("Bearer " + SERIALIZED_JWT);
    }

    @Test
    public void shouldIncludeMissingClaim() throws Exception {
        when(jwtValidator.readAndValidate(SERIALIZED_JWT)).thenThrow(new MissingRequiredClaimException(JwtClaims.RegisteredClaim.EXPIRY));
        when(jwtValidator.determineUnverifiedIssuer(SERIALIZED_JWT)).thenReturn(Optional.empty());

        expectedException.expect(PermanentAuthenticationFailedException.class);
        expectedException.expectMessage("Failed to authenticate request from unknown issuer: MissingRequiredClaimException - EXPIRY");

        requestAuthenticator.authenticateRequest("Bearer " + SERIALIZED_JWT);
    }

    @Test
    public void shouldIncludeMissingHeader() throws Exception {
        when(jwtValidator.readAndValidate(SERIALIZED_JWT)).thenThrow(new MissingRequiredHeaderException(JwsHeader.Header.KEY_ID));
        when(jwtValidator.determineUnverifiedIssuer(SERIALIZED_JWT)).thenReturn(Optional.empty());

        expectedException.expect(PermanentAuthenticationFailedException.class);
        expectedException.expectMessage("Failed to authenticate request from unknown issuer: MissingRequiredHeaderException - KEY_ID");

        requestAuthenticator.authenticateRequest("Bearer " + SERIALIZED_JWT);
    }

    @Test
    public void shouldFailWithTransientErrorIfCannotRetrieveKey() throws Exception {
        when(jwtValidator.readAndValidate(SERIALIZED_JWT)).thenThrow(new CannotRetrieveKeyException("500 error from key server"));
        when(jwtValidator.determineUnverifiedIssuer(SERIALIZED_JWT)).thenReturn(Optional.empty());

        expectedException.expect(TransientAuthenticationFailedException.class);
        expectedException.expectMessage("Failed to retrieve the key required to authenticate request from unknown issuer: 500 error from key server");

        requestAuthenticator.authenticateRequest("Bearer " + SERIALIZED_JWT);
    }

    @Test
    public void shouldReturnJwtIfSuccessful() throws Exception {
        when(jwtValidator.readAndValidate(SERIALIZED_JWT)).thenReturn(jwt);

        Jwt validatedToken = requestAuthenticator.authenticateRequest("Bearer " + SERIALIZED_JWT);

        assertEquals(jwt, validatedToken);

        verify(jwtValidator).readAndValidate(SERIALIZED_JWT);
    }
}
