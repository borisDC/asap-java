package com.atlassian.asap.core.server.filter;

import com.atlassian.asap.api.Jwt;
import org.apache.http.HttpStatus;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filters requests that contain authentic JWT tokens based on an authorization policy.
 */
public abstract class AbstractRequestAuthorizationFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // nothing to do
    }

    @Override
    public final void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        // will fail if the request is not an HTTP request
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        // get the authentic JWT token
        Jwt jwt = (Jwt) httpRequest.getAttribute(AbstractRequestAuthenticationFilter.AUTHENTIC_JWT_REQUEST_ATTRIBUTE);
        if (jwt == null) {
            onTokenNotFound(httpRequest, httpResponse, filterChain);
        } else if (isAuthorized(httpRequest, jwt)) {
            onAuthorizationSuccess(jwt, httpRequest, httpResponse, filterChain);
        } else {
            onAuthorizationFailure(jwt, httpRequest, httpResponse, filterChain);
        }
    }

    /**
     * Override this method to define the behaviour of the filter when a token is not found in the request.
     * The default behaviour is to throw {@link IllegalStateException} since it is assumed that this filter is
     * behind the {@link AbstractRequestAuthenticationFilter}.
     *
     * @param request     HTTP request
     * @param response    HTTP response
     * @param filterChain Web filter chain
     * @throws IOException      see exception definition
     * @throws ServletException see exception definition
     */
    protected void onTokenNotFound(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        throw new IllegalStateException(
                "Request authorization filter requires an authentic JWT attribute in the request. " +
                "Have you added the authentication filter to the chain?");
    }

    /**
     * Override this method to define the behaviour of the filter when a valid token is found, but the request is not
     * authorized. The default behaviour is to return a 403 FORBIDDEN error.
     *
     * @param jwt         Authentic and valid JWT token extracted from the request
     * @param request     HTTP request
     * @param response    HTTP response
     * @param filterChain Web filter chain
     * @throws IOException      see exception definition
     * @throws ServletException see exception definition
     */
    protected void onAuthorizationFailure(Jwt jwt, HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {
        response.sendError(HttpStatus.SC_FORBIDDEN);
    }

    /**
     * Override this method to define the behaviour of the filter when a valid token is found and the request is authorized.
     * The default behaviour is to let the request continue down the filter chain.
     *
     * @param request     HTTP request
     * @param response    HTTP response
     * @param filterChain Web filter chain
     * @throws IOException      see exception definition
     * @throws ServletException see exception definition
     */
    protected void onAuthorizationSuccess(Jwt jwt, HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {
        filterChain.doFilter(request, response);
    }

    /**
     * Decides if the token is authorized for the request.
     *
     * @param request HTTP request received
     * @param jwt     Authentic and valid JWT token extracted from the request
     * @return true if the token is authorized for the request
     */
    protected abstract boolean isAuthorized(HttpServletRequest request, Jwt jwt);

    @Override
    public void destroy() {
        // nothing to do
    }
}
