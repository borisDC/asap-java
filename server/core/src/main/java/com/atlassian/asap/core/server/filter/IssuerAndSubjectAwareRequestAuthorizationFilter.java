package com.atlassian.asap.core.server.filter;

import com.atlassian.asap.api.Jwt;

import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

/**
 * This filter takes a map of issuers and predicates that define which issuers should be allowed to make ASAP requests
 * and what the subjects for those issuers should look like.
 *
 * <p>If a request does not specify the subject, the subject is assumed to be the issuer.
 *
 * <p>A concrete use case for this would be if you have a service that is only allowed to specify certain users as the
 * subject. For example, if {@code "impersonation-service"} is allowed to use any {@code @atlassian.com} users as the
 * subject, you could define a set of rules like this:
 *
 * <pre>{@code
 *      ImmutableMap.of("impersonation-service", Pattern.compile(".*@atlassian\\.com").asPredicate())
 * }</pre>
 */
public class IssuerAndSubjectAwareRequestAuthorizationFilter extends RulesAwareRequestAuthorizationFilter {
    public IssuerAndSubjectAwareRequestAuthorizationFilter(Map<String, Predicate<String>> issuersAndSubjectChecks) {
        super(toJwtRules(issuersAndSubjectChecks));
    }

    /**
     * A helper constructor to construct a filter from a list of issuers.
     *
     * <p>Issuers cannot impersonate anybody. The issuer must be the same as the subject. (The subject may be blank, in
     * which case we treat the issuer as the subject).
     *
     * @param issuers A list of issuers to whitelist
     * @return A filter that only accepts self-signed JWTs from the listed issuers
     */
    public static IssuerAndSubjectAwareRequestAuthorizationFilter issuers(Set<String> issuers) {
        return new IssuerAndSubjectAwareRequestAuthorizationFilter(issuerAndSubjectMatches(issuers));
    }

    private static Map<String, Predicate<String>> issuerAndSubjectMatches(Set<String> issuers) {
        return issuers.stream().collect(toMap(
                identity(),
                issuer -> issuer::equals
        ));
    }

    private static Map<String, Predicate<Jwt>> toJwtRules(Map<String, Predicate<String>> subjectRules) {
        return subjectRules.entrySet().stream().collect(toMap(
                Map.Entry::getKey,
                entry -> jwt -> entry.getValue().test(jwt.getClaims().getSubject().orElse(jwt.getClaims().getIssuer()))
        ));
    }
}
