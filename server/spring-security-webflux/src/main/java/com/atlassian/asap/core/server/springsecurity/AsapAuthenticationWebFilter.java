package com.atlassian.asap.core.server.springsecurity;

import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.server.authentication.AuthenticationWebFilter;
import org.springframework.security.web.server.util.matcher.ServerWebExchangeMatcher;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.function.Function;

/**
 * creates {@code AuthenticationWebFilter} using provided {@code Function<ServerWebExchange, Mono<Authentication>>}
 * authenticationConverter and {@code ServerWebExchangeMatcher} matcher.
 * {@code ServerWebExchangeAuthenticationConverter} and {@code ServerWebExchangeBearerTokenRequestMatcher} are used as default converter
 * and matcher appropriately.
 *
 * @see ServerWebExchangeAuthenticationConverter
 * @see ServerWebExchangeBearerTokenRequestMatcher
 */
public class AsapAuthenticationWebFilter extends AuthenticationWebFilter {
    private Function<ServerWebExchange, Mono<Authentication>> authenticationConverter = new ServerWebExchangeAuthenticationConverter();
    private ServerWebExchangeMatcher requiresAuthenticationMatcher = new ServerWebExchangeBearerTokenRequestMatcher();

    public AsapAuthenticationWebFilter(ReactiveAuthenticationManager authenticationManager) {
        super(authenticationManager);
        super.setAuthenticationConverter(authenticationConverter);
        super.setRequiresAuthenticationMatcher(requiresAuthenticationMatcher);
    }
}
