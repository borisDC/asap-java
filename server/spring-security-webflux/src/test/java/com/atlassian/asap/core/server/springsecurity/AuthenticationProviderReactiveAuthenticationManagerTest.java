package com.atlassian.asap.core.server.springsecurity;

import org.junit.Test;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderNotFoundException;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.core.Authentication;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AuthenticationProviderReactiveAuthenticationManagerTest {

    @Test(expected = IllegalArgumentException.class)
    public void itShouldThrowIllegalArgumentExceptionIfProviderListIsEmpty() {
        new AuthenticationProviderReactiveAuthenticationManager(Collections.emptyList());
    }

    @Test(expected = IllegalArgumentException.class)
    public void itShouldThrowIllegalArgumentExceptionIfProviderListIsNull() {
        new AuthenticationProviderReactiveAuthenticationManager((List<AuthenticationProvider>) null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void itShouldThrowIllegalArgumentExceptionIfProviderVarargIsNull() {
        new AuthenticationProviderReactiveAuthenticationManager((AuthenticationProvider) null);
    }

    @Test
    public void itShouldThrowProviderNotFoundExceptionIfProvidersDoesnotSupportGivenAuth() {
        AuthenticationProvider provider = mock(AuthenticationProvider.class);
        when(provider.supports(UnverifiedBearerToken.class)).thenReturn(false);

        assertThatExceptionOfType(ProviderNotFoundException.class).isThrownBy(() -> {
            new AuthenticationProviderReactiveAuthenticationManager(Collections.singletonList(provider))
                    .authenticate(new UnverifiedBearerToken("abc-123")).block();
        });
        verify(provider).supports(UnverifiedBearerToken.class);
    }

    @Test
    public void itShouldCallFirstMatchedProviderForGivenAuthType() {
        UnverifiedBearerToken expected = new UnverifiedBearerToken("abc-123") {
            @Override
            public boolean isAuthenticated() {
                return true;
            }
        };

        AuthenticationProvider provider1 = mock(AuthenticationProvider.class);
        when(provider1.supports(UnverifiedBearerToken.class)).thenReturn(true);
        when(provider1.authenticate(any(UnverifiedBearerToken.class))).thenReturn(expected);

        AuthenticationProvider provider2 = mock(AuthenticationProvider.class);
        when(provider2.supports(UnverifiedBearerToken.class)).thenReturn(true);
        when(provider2.authenticate(any(UnverifiedBearerToken.class))).thenReturn(expected);

        ReactiveAuthenticationManager manager = new AuthenticationProviderReactiveAuthenticationManager(Arrays.asList(provider1, provider2));
        Authentication actual = manager.authenticate(new UnverifiedBearerToken("abc-123")).block();

        assertEquals(expected, actual);
        verify(provider1).supports(UnverifiedBearerToken.class);
        verify(provider1).authenticate(any(UnverifiedBearerToken.class));
        verify(provider2, never()).supports(UnverifiedBearerToken.class);
        verify(provider2, never()).authenticate(any(UnverifiedBearerToken.class));
    }
}
