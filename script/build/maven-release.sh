#!/bin/sh
set -e
set -x

# Prepare and perform the maven release
mvn -B release:prepare -DscmCommentPrefix="[skip ci] " -DautoVersionSubmodules=true
# Release the artifact to p.a.c
mvn release:perform -DlocalCheckout=true
