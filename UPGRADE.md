# Upgrade notes

## Version 2.21 from 2.20
* new `spring-security-webflux` artifact that provides WebFlux Spring Security Support was added. It uses HTTP authorization 
header to perform ASAP authentication & authorization in Spring 5 reactive context. 
 
## Version 2.20 from 2.19

* New `mandatory` attribute on `com.atlassian.asap.core.server.jersey.Asap` annotation to handle no or other type of 
 authentication in parallel to ASAP
* `com.atlassian.asap.core.server.jersey.AsapValidator` is now an interface (and the default implementation has been
moved to `com.atlassian.asap.core.server.jersey.WhiteListAsapValidator`) 
* `com.atlassian.asap.core.keys.publickey.ChainedKeyProvider` is deprecated in favor of the generic
`com.atlassian.asap.core.keys.ChainedKeyProvider`

## Version 2.19 from 2.18

* New `asap-client-spring-webflux` artifact that provides an `AsapExchangeFilterFunction` for Spring Webflux WebClient
that adds the Authorization header for ASAP.

## Version 2.18 from 2.17

* Feign (in `asap-client-feign`) has been upgraded from 8.x to 9.x, which involved a change in group ID (from
  `com.netflix.feign` to `io.github.openfeign`). Artifact IDs and packages remain the same.

## Version 2.17 from 2.15

* Version 2.16(.0) was accidentally burnt. Skipping to 2.17.x.
* The `asap-server-jersey2` artifact uses standard APIs and avoids internal Jersey classes to facilitate
  upgrading Jersey (or using another JAX-RS implementation!). Also upgraded to JAX-RS 2.1. 
* Removed the `JwtParam` annotation. To get a reference to the `Jwt`, receive a `ContainerRequestContext` by
  injection and downcast the `SecurityContext` to a `JwtSecurityContext`.
* New `AsapInterceptor` in `asap-client-spring` artifact that provides spring's own http client support for ASAP.
* The String-taking deprecated methods of `FailureHandler` in the Jersey 2 integration have been removed. The
  alternative is to use the methods that take an `AuthenticationFailedException` instead.
* Point version bump of Spring Framework version to 4.3.14 and minor version bump of Spring Security to 4.2.4.
  Please refer to the upgrade guides of
  [Spring Security 4.1](https://docs.spring.io/spring-security/site/docs/4.1.x/reference/htmlsingle/#new) and
  [Spring Security 4.2](https://docs.spring.io/spring-security/site/docs/4.2.x/reference/htmlsingle/#new). 

## Version 2.15 from 2.14

* New `asap-client-okhttp3` artifact that provides an `AsapInterceptor` for OkHttp 3 that adds the Authorization header for ASAP

## Version 2.14 from 2.13

* Introduced a new rules-based authorisation filter, `RulesAwareRequestAuthorizationFilter`.
* Deprecated the single argument constructor for `WhitelistRequestAuthorizationFilter`. This fixes a bug related to the
  intended behaviour of the constructor versus the actual behaviour. Users of the library should use
  `IssuerAndSubjectAwareRequestAuthorizationFilter#issuers` instead.

## Version 2.13 from 2.12

* `AsapAuth` now supports both 'issuer' and 'impersonationIssuer' refer to javadoc for more info
* `subjectImpersonation` field in `AsapAuth` is deprecated

## Version 2.12 from 2.11.4

* `AuthenticationContext` now supports multiple audiences. The method `getResourceServerAudience` returning a single
audience has been deprecated in favor of `getResourceServerAudiences` that returns a set. 

## Version 2.11 from 2.10.4
* Upgrades Nimbus JWT library to 4.34.1 from 3.10.

## Version 2.10.2 from 2.10

* `@Asap` annotation in `asap-server-jersey2` is now `@Inherited`
* `@Asap` annotation has a `enabled` property which can now be set to `false` to switch off ASAP authentication
  for a particular method or subclass

## Version 2.10 from 2.9

* Spring framework version has been upgraded from 4.2 to 4.3. This only affects applications using the Spring integration.

## Version 2.9 from 2.8

* Updates the Jersey 2 failure handler (from `asap-server-jersey2`) to allow different handling of transient and non-transient failures. The change is backward
  compatible and does not require any work unless to take advantage of the new possibility. Note that some methods of `FailureHandler` have been deprecated. 

## Version 2.8 from 2.7

* Introduces `asap-service-api` and `asap-service-core`.  These are optional modules meant for large applications with multiple ASAP integration points to use
  to simplify their integration with ASAP.  Their use is not mandatory, and none of the existing modules had any breaking changes.

## Version 2.7 from 2.6

* AuthenticationFailureExceptions raised from `RequestAuthenticator` (ie. `PermanentAuthenticationFailureException` and `TransientAuthenticationFailureException`)
  will now include additional information about the failure, rather than a generic message.  This will include the class of failure (including which claim failed),
  plus the apparent (but un-verified) issuer of the token (if one was able to be extracted from the Jwt).
* An additional method `determineUnverifiedIssuer` was added to the `JwtValidator` interface to support this non-verifying issuer claim retrieval requirement. 

## Version 2.6 from 2.5

* JWT tokens can have custom claims. Most applications will not be affected, but if your
  application is providing its own implementations of `Jwt` then you will have to add the new
  methods.
* `asap-common` now depends on `javax.json`. A reference implementation has been added as
  a dependency. If you are deploying ASAP in an environment that already provides an implementation
  of JSR 353 you may want to exclude the reference implementation.
* `JwtClaims.Claim` has been renamed to `JwtClaims.RegisteredClaim`.

## Version 2.5 from 2.4

* When authenticating a JWT, a failure to retrieve the required public key will now throw a TransientAuthenticationFailedException
  (previously it would throw the generic AuthenticationFailedException, whereas TransientAuthenticationFailedException is a new subclass
  of this).  The intention here is to be able to distinguish a transient failure to verify the JWT from failures caused by errors in the
  JWT claims.
  
  A point to note in this change is that the default implementation of `AbstractRequestAuthenticationFilter` now returns a 500-InternalServerError
  to the client instead of the previous 401-Unauthorized error in this case.  This default behaviour is embedded in the newly added
  `onAuthenticationError()` handler method, and can be overridden accordingly if the previous behaviour is specifically desired.
  
  Similarly, for Jersey 1 implementations, the previous `AuthenticationFailedExceptionMapper` class has been split into two, a
  `PermanentAuthenticationFailedException` and a `TransientAuthenticationFailedExceptionMapper`, which will handle 401-Unauthorized and
  500-InternalServerError responses to the client accordingly.

## Version 2.4 from 2.3

* `JwtValidatorImpl` now has a new constructor to allow validation against a number
  of audience values instead of a single one.
* `JwtClaimsValidator` changes `validate` method to take a set of strings for audience,
  instead of a single one.

## Version 2.3 from 2.2

* `JwtValidator` has become an interface, and the implementation has been renamed
to `JwtValidatorImpl`. Applications that were instantiating this class directly
have to change the class name to `JwtValidatorImpl`.

## Version 2.2 from 2.1

* In the Jersey 1.x integration, the `AsapAuthenticationFilter` now takes a JWT
prototype instead of the properties of a JWT token. Applications using this filter
need to build a `Jwt` instance to use it as a prototype.

## Version 2.1 from 2.0

This release is binary and source compatible with the previous version.

## Version 2.0 from 1.x

* The library has been repackaged as multiple Maven artifacts to avoid unnecessary dependencies on third party
libraries. ASAP client applications will have to replace the dependency on `asap-java` with `asap-common`, `asap-client-core`
and possibly some of the framework-dependent client-side artifacts, like `asap-client-jersey1` for Jersey 1.x. ASAP
server applications will have to replace the dependency on `asap-java` with `asap-common`, `asap-server-core` and some
of the framework-dependent server-side artifacts, like `asap-server-spring`. Most of the classes have not changed their
package names.

* `AbstractRequestAuthenticatorFilter` has been renamed to `AbstractRequestAuthenticationFilter`.

* `RequestAuthenticator` now performs exclusively token validation and authentication, but it no longer performs
authorization. Any valid token will be accepted, regardless of the subject or the issuer. Applications should
complement this with a sensible authorization policy. Previous versions of the library used to authorize requests based
on an `issuer==subject` constraint and a whitelist of valid subjects. This check is no longer performed by
`RequestAuthenticator`.

* Because from `RequestAuthenticator` of the elimination of the responsibility to authorize the request,
`AbstractRequestAuthenticationFilter` now only performs authentication. A new filter hierarchy
(`AbstractRequestAuthorizationFilter`) has been introduced to allow applications to implement a sensible
authorization policy.

* The library now requires at least Java 8.

* Uses of Fugue `Option` have been replaced with `java.util.Optional` in all the APIs.

* Joda-time has been removed as a dependency and replaced with similar time APIs introduced in Java 8.

